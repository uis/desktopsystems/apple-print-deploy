# Print Deploy

Scripts & configuration for building printers for Print Deploy on macOS

## Commands

Build list of PPD files required from input file:

`cut -d, -f 2 Konica.csv | sort | uniq > printer_list.txt`

Expand pkg to access contents. (Destination must not exist)

`pkgutil --expand-full /Volumes/IT6BW/750i_650i_306i_4750i_4700i_11.pkg ~/Support/temp/IT6BW`
