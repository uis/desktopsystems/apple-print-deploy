#!/usr/local/uis/bin/python3

#######################################
# Build printers for Print Deploy     #
# JWRN3                               #
#######################################

#############
# Misc info #
#############
__author__ = "jwrn3@cam.ac.uk"

###########################
# Import modules required #
###########################
import subprocess
import argparse
import csv
from pathlib import Path
import time


def parse_arguments():

    # Handle arguments passes at CLI
    parser = argparse.ArgumentParser(
        description="Build printers from source file."

    )
    parser.add_argument(
        "--file",
        action="store",
        dest="config_file",
        help="printer configuration file in csv format",
        required=True,
    )
    arguments = parser.parse_args()

    return (
        arguments.config_file
    )


def main():
    # Get params passed to the script
    config_file = parse_arguments()

    # Open config file and loop through each line as csv
    try:
        with open(config_file, newline='') as file:
            csvreader = csv.reader(file)

            # Skip header
            #next(csvreader, None) 

            # Init counter
            line = 0

            # Loop through each row
            for row in csvreader:

                print()

                # Increment counter
                line += 1

                # Skip commented entries
                if row[0].startswith("#"):
                    print (f"Skipping line {line}")
                    continue

                # Unpack each row and assign to vars
                # printer_opts is optional list of printer options
                url, model, ppd, *printer_opts = row

                # Extract queue from url
                # split on last /
                # if queue[0] is not None then delimiter is present
                # and can split
                # TODO: regex to parse URL correctly
                queue = url.rpartition('/')
                if queue[0]:
                    queue = queue[-1]
                else:
                    continue
                
                # Check all required fields are present
                # TODO: perform some proper error checking
                if len(ppd) == 0 or len(url) == 0 or len(model) == 0:
                    print(f"Error: url, model & ppd are all required attributes. Error in config file on line {line}")
                    continue

                # Print queue details
                print(f"Queue: {queue}")
                print(f"URL: {url}")
                print(f"Model: {model}")
                print(f"PPD: {ppd}")
               
                # Create command list
                lpadmin_command = ["/usr/sbin/lpadmin",
                                   "-p",
                                   queue,
                                   "-P",
                                   ppd,
                                   "-v",
                                   url,
                                   "-E",
                                   ]

                # If printer_list is populated then extend lpadmin_command
                # with opt prefixed with "-o"
                for opt in printer_opts:
                    if opt:
                        print(f"Option: {opt}")
                        lpadmin_command.extend(["-o", opt])

                # Check driver exists, if not skip to next queue
                path = Path(ppd)
                if not path.is_file():
                    print(f"{ppd} does not exist!")
                    continue

                # Build printer and suppress errors to DEVNULL
                # only because CUPS gives PPD warning each time
                try:
                    subprocess.run(
                        lpadmin_command, stderr=subprocess.DEVNULL
                    )

                # Handle errors
                except subprocess.CalledProcessError as error:
                    print(error.returncode, error.output)

                except OSError as error:
                    print(error.strerror)

                else:
                    # Success!
                    print(f"Installation of {queue} now complete")

                # Bodge to let things coalesce
                time.sleep(1)

    # Input file doesn't exist
    except FileNotFoundError:
        print(f"Error: {config_file} does not exist!")
        quit(1)


if __name__ == "__main__":
    main()
